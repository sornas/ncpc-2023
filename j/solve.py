def main():
    n, m  = list(map(int, input().split()))
    aa = list(map(int, input().split()))

    aa.sort()
    scouts = [0 for _ in range(m)]
    for i in range(m):
        #jprint(i, aa, scouts)
        if not aa:
            break
        scouts[i] = aa.pop()

    scouts.sort()
    if n > m:
        for i in range(m):
            #jprint(i, aa, scouts)
            if not aa:
                break
            scouts[i] += aa.pop()
        scouts.sort()
    print(scouts[-1])

main()
