def main():
    romans = {
        "I": 1,
        "V": 5,
        "X": 10,
        "L": 50,
        "C": 100,
        "D": 500,
        "M": 1000,
    }

    n = int(input())
    for _ in range(n):
        s = input()
        
        positive = [True for _ in range(len(s))]
        sum = 0
        sn = [romans[c] for c in s]
        # print(sn)

        largest = 0

        for n in sn[::-1]:
            if n > largest:
                largest = n
                sum += n
            elif n == largest:
                sum += n
            else:
                sum -= n
        print(sum)
        
        #nums=[]
        # for i, c in enumerate(s):
        #     if any(romans[later] > romans[c] for later in s[i+1:]):
        #         nums.append(-romans[c])
        #     else:
        #         nums.append(romans[c])
        #print(sum(nums))

main()
