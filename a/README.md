## A: aperiodic appointments

We started by trying to find a formula for calculating the length of the string.
Let's start with k=3:

```
1: 000            ; first, three 0s
2: 0001           ; 0 repeated
3: 00010001
4: 000100010001
5: 0001000100011  ; 0001 repeated three times
6: 0001000100011 0001000100011 0001000100011 1  ; 0001000100011 repeated three times
```

The length of the string can be written as follows.

- 2: `0001` => $`k+1`$
- 4: `000100010001` => $`k(k+1)`$
- 5: `0001000100011` => $`k(k+1)+1`$
- 6: `0001000100011(x2)1` => $`k(k(k+1)+1)+1`$

We write this in a tree-like structure that maps the expressions like this:

- `0001` => $`k+1`$ => `[k, [], T]`
- `000100010001` => $`k(k+1)`$ => `[k, [k, [], T], F]`
- `0001000100011` => $`k(k+1)+1`$ => `[k, [k, [], T], T]`
- `0001000100011(x2)1` => $`k(k(k+1)+1)+1`$ => `[k, [k, [k, [], T], T], T]`

As you can see, every "node" contains three values; 1: how many times the inner
block repeats (the **coefficient**), 2: the inner block that is repeated, and 3:
if we have $`+1`$ or not. The empty list means that we only have a zero that
repeats. (In the implementation this was `None` instead.)

At this point we realized that we could find the amount of ones in the string
from this representation as well.

```py
def count_len(tree):
    if tree == None:
        # a single 0
        return 1
    return tree[0] * count_n(tree[1]) + int(tree[2])

def count_one(tree):
    if tree[1] == None:
        return int(tree[2])
    else:
        return tree[0] * count_one(tree[1]) + int(tree[2])
```

We can now generate an expression for the length of the string. We defined our
algorithm as follows:

1. Increase the depth of $`k(k+1)+1`$-structures until the entire length exceeds
   $`n`$.
2. For the outermost node, find the largest coefficient such that the length of
   the string does not exceed $`n`$.
3. Add a copy of the next inner (repeated) node and go to step 2 but changing
   the copied.

Any time we change the expression we check if the length is equal to $`n`$, at
which point we stop.

Step 3 in particular is pretty abstract. Let's go through sample input 2 using
the above algorithm.

```
n = 99, k = 5

1: increase depth
-----------------

k           => l =   5 < 99
k+1         => l =   6 < 99
k(k+1)      => l =  30 < 99
k(k+1)+1    => l =  31 < 99
k(k(k+1)+1) => l = 155 > 99

now we substitute k=5 and get

5(5(5+1)+1)

2: find largest coefficient
---------------------------

5(5(5+1)+1) => l = 155 > 99
4(5(5+1)+1) => l = 124 > 99
3(5(5+1)+1) => l =  93 < 99

3: add copy and repeat
----------------------

3(5(5+1)+1) + 5(5+1)+1 => l = 124 > 99
3(5(5+1)+1) + 5(5+1)   => l = 123 > 99
3(5(5+1)+1) + 4(5+1)   => l = 117 > 99
3(5(5+1)+1) + 3(5+1)   => l = 111 > 99
3(5(5+1)+1) + 2(5+1)   => l = 105 > 99
3(5(5+1)+1) + 1(5+1)   => l =  99 = 99 done!
```

A VERY rough complexity analysis shows that when n is large, repeatedly
multiplying $`k`$ with itself makes the length increase exponentially, meaning
we do a logarithmic amount of work in step 1. Calculating the length of the
string and amount of ones is done logarithmically. Finding the coefficients can
be done with a binary search, making step 2 logarithmic too.

The case we missed during the competition was that at some point, the string
just contains 1 forever. Here's k=2:

```
001
0010011
00100111...  since 11 was repeated
```

So we had to check that in step 1 of the algorithm. If we have reached depth
$`k`$, we just count the number of 1s so far and add $`n-k`$ more 1s.
