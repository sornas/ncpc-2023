import sys
from copy import deepcopy

def main():
    n, k = list(map(int, input().split()))

    #tree = [3, [5, [5, None, True], True], False]
    # tree = [1, [5, None, True], False]
    
    def count_n(tree):
        if tree == None:
            return 1
        return tree[0] * count_n(tree[1]) + int(tree[2])

    def count_one(tree):
        if tree[1] == None:
            return int(tree[2])
        else:
            return tree[0] * count_one(tree[1]) + int(tree[2])

    def depth(tree):
        if tree[1] == None:
            return 1
        return depth(tree[1]) + 1

    node = [k, None, False]
    while count_n(node) < n:
        if node[2] == False:
            node[2] = True
        else:
            # repeating 1s after depth == k
            if depth(node) == k:
                print(count_one(node) + (n - count_n(node)))
                sys.exit(0)
            new_node = [k, node, False]
            node = new_node

    #print(node)

    if count_n(node) == n:
        print(count_one(node))
        sys.exit(0)

    nodes = [node]

    def count_n_nodes():
        return sum(count_n(node) for node in nodes)
    
    def count_one_nodes():
        return sum(count_one(node) for node in nodes)

    # find largest node[0] where count_n(node) < n, binary search
    def set_largest_lower(inner_node):
        #print("set largest lower", inner_node)
        lo = 0
        hi = k
        while True:
            #print(lo, hi)
            mi = (lo + hi) // 2
            inner_node[0] = mi
            s = count_n_nodes()
            if s == n:
                print(count_one_nodes())
                sys.exit(0)
            if s < n:
                lo = mi
            else:
                hi = mi
            if lo + 1 == hi:
                #print("set largest lower done", lo, hi)
                inner_node[0] = lo
                return

    set_largest_lower(node)
    if count_n(node) == n:
        print(count_one(node))
        sys.exit(0)
    #print(node, count_one(node))
    while True:
        #print(nodes)
        nodes.append(deepcopy(node[1]))
        node = nodes[-1]
        node[2] = False
        if count_n_nodes() == n:
            print(count_one_nodes())
            sys.exit(0)
        set_largest_lower(node)
        if count_n_nodes() == n:
            print(count_one_nodes())
            sys.exit(0)


main()
