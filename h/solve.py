def main():
    data = { "heroes": [ { "name": "Shadow", "energy_cost": 4, "power_level": 6, "ability": "" }, { "name": "Gale", "energy_cost": 3, "power_level": 5, "ability": "" }, { "name": "Ranger", "energy_cost": 2, "power_level": 4, "ability": "" }, { "name": "Anvil", "energy_cost": 5, "power_level": 7, "ability": "" }, { "name": "Vexia", "energy_cost": 2, "power_level": 3, "ability": "" }, { "name": "Guardian", "energy_cost": 6, "power_level": 8, "ability": "" }, { "name": "Thunderheart", "energy_cost": 5, "power_level": 6, "ability": "Phalanx - If the location this card is played at has 4 friendly cards, including this one, then its power is doubled. Note that other buffs the card receives are not doubled." }, { "name": "Frostwhisper", "energy_cost": 1, "power_level": 2, "ability": "" }, { "name": "Voidclaw", "energy_cost": 1, "power_level": 3, "ability": "" }, { "name": "Ironwood", "energy_cost": 1, "power_level": 3, "ability": "" }, { "name": "Zenith", "energy_cost": 6, "power_level": 4, "ability": "Centered Focus - If this card is played at the center location, +5 power." }, { "name": "Seraphina", "energy_cost": 1, "power_level": 1, "ability": "Celestial Healing - Seraphina grants +1 power to each friendly card at this location, other than itself. This includes other Seraphina cards." } ] }
    power_levels = {
        hero["name"]: hero["power_level"]
        for hero in data["heroes"]
    }
    #print(power_levels)

    def line():
        _, *cards = input().split()
        cards = [[card, power_levels[card]] for card in cards]
        return cards

    lp1 = line()
    lp2 = line()
    cp1 = line()
    cp2 = line()
    rp1 = line()
    rp2 = line()

    all = [lp1, lp2, cp1, cp2, rp1, rp2]

    # thunderheart
    def thunderheart(cards):
        if len(cards) == 4:
            # apply *2 to thunderheart
            for i in range(4):
                if cards[i][0] == "Thunderheart":
                    cards[i][1] *= 2

    # +(len-1) for each seraphina, separate
    def count_seraphina(cards):
        c = 0
        for card in cards:
            if card[0] == "Seraphina":
                c += 1
        return c

    # only call this for center
    def zenith(cards):
        for i in range(len(cards)):
            if cards[i][0] == "Zenith":
                cards[i][1] += 5

    for l in all:
        thunderheart(l)
    zenith(cp1)
    zenith(cp2)

    pllp1 = sum(card[1] for card in lp1)
    pllp2 = sum(card[1] for card in lp2)
    plcp1 = sum(card[1] for card in cp1)
    plcp2 = sum(card[1] for card in cp2)
    plrp1 = sum(card[1] for card in rp1)
    plrp2 = sum(card[1] for card in rp2)

    if (c := count_seraphina(lp1)) > 0:
        pllp1 += c * (len(lp1) - 1)
    if (c := count_seraphina(lp2)) > 0:
        pllp2 += c * (len(lp2) - 1)
    if (c := count_seraphina(cp1)) > 0:
        plcp1 += c * (len(cp1) - 1)
    if (c := count_seraphina(cp2)) > 0:
        plcp2 += c * (len(cp2) - 1)
    if (c := count_seraphina(rp1)) > 0:
        plrp1 += c * (len(rp1) - 1)
    if (c := count_seraphina(rp2)) > 0:
        plrp2 += c * (len(rp2) - 1)

    locs1 = 0
    locs2 = 0

    if pllp1 > pllp2:
        locs1 += 1
    elif pllp1 < pllp2:
        locs2 += 1

    if plcp1 > plcp2:
        locs1 += 1
    elif plcp1 < plcp2:
        locs2 += 1

    if plrp1 > plrp2:
        locs1 += 1
    elif plrp1 < plrp2:
        locs2 += 1

    if locs1 > locs2:
        print("Player 1")
    elif locs1 < locs2:
        print("Player 2")
    else:
        # check tie-break
        sum_pl1 = pllp1 + plcp1 + plrp1
        sum_pl2 = pllp2 + plcp2 + plrp2
        if sum_pl1 > sum_pl2:
            print("Player 1")
        elif sum_pl1 < sum_pl2:
            print("Player 2")
        else:
            print("Tie")

main()
