def main():
    prev = 0
    test = 1

    while True:
        print(f"buf[{test}]")
        ans = int(input())
        if ans == 0:
            break
        prev = test
        test *= 2

    # found first =0, binary search backwards

    """
    int binarySearch() {
        // want: P(lo) is always false, and P(hi) always true
        int lo, hi, mi; lo=0; hi=n-1;
        while(lo+1<hi) {
            mi=(lo+hi)/2;
            if(P(mi)) hi=mi; else lo=mi;
        }
        return hi;
    }
    """

    lo = prev
    hi = test

    if lo + 1 == hi:
        print("strlen(buf) = 2")
        return

    while True:
        mi = (lo + hi) // 2
        print(f"buf[{mi}]")
        ans = int(input())
        if ans != 0:
            lowbound = mi
            highbound = hi
        else:
            lowbound = lo
            highbound = mi
        if lowbound + 1 == highbound:
            print(f"strlen(buf) = {lowbound+1}")
            break
        else:
            lo = lowbound
            hi = highbound

main()
