from math import prod

def main():
    PRIMES = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997, 1009, 1013, 1019, 1021, 1031, 1033, 1039, 1049, 1051, 1061, 1063, 1069, 1087, 1091, 1093, 1097, 1103, 1109, 1117, 1123, 1129, 1151, 1153, 1163, 1171, 1181, 1187, 1193, 1201, 1213, 1217, 1223]
    N = int(input())
    tree = [[] for _ in range(N)]
    for _ in range(N-1):
        a, b = list(map(int, input().split()))
        tree[a].append(b)
        tree[b].append(a)

    parents = {1: None}
    children = [[] for _ in range(N+1)]
    # find all to one
    q = [1]
    while q:
        n = q.pop()
        for ns in tree[n]:
            if ns not in parents:
                parents[ns] = n
                children[n].append(ns)
                q.append(ns)

    factor = [None for _ in range(N+1)]
    factor[1] = 1
    depth = [None for _ in range(N+1)]

    def set_depth(n, d):
        if factor[n] is None:
            depth[n] = d
            for child in children[n]:
                set_depth(child, d + 1)
        else:
            depth[n] = 0
            for child in children[n]:
                set_depth(child, 1)

    def all_marked():
        return all(factor[n] is not None for n in range(1, N+1))

    while not all_marked():
        # find longest leaf to marked
        set_depth(1, 1)
        deepest = 1
        for n in range(2, N+1):
            if depth[n] > depth[deepest]:
                deepest = n
        # mark it and all ancestors with lowest available prime
        p = PRIMES.pop(0)
        while factor[deepest] is None:
            factor[deepest] = p
            deepest = parents[deepest]

    # instead of factors, actual marks
    marks = [0 for _ in range(N+1)]
    def set_mark(n, f):
        marks[n] = f * factor[n]
        for child in children[n]:
            set_mark(child, marks[n])

    set_mark(1, 1)

    print(" ".join(str(marks[i]) for i in range(1, N+1)))

main()
