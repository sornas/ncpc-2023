Solved and attempted problems during [NCPC
2023](https://nordic.icpc.io/ncpc2023/). Some problems have writeups in their
respective directory.

Team members:

- Henry Andersson
- Martin Högstedt
- Gustav Sörnäs

## Solved (in order)

- C (converting romans)
- J (jamboree)
- D (die hard)
- K (karl coder)
- H (heroes of velmar)

## Attempted

- A: We missed that the 1s repeat infinitely.
- F: We realized that we had to mark with different primes, but didn't get much
  further than that.
